<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AccountsTableDataSeeder extends Seeder
{
    public function run()
    {
        DB::table('accounts')->insert([
            ['name' => 'Account 1', 'balance' => 1000,'created_at'=>Carbon::now()],
            ['name' => 'Account 2', 'balance' => 2000,'created_at'=>Carbon::now()],
            ['name' => 'Account 3', 'balance' => 3000,'created_at'=>Carbon::now()],
            ['name' => 'Account 4', 'balance' => 4000,'created_at'=>Carbon::now()],
            ['name' => 'Account 5', 'balance' => 5000,'created_at'=>Carbon::now()],
        ]);
    }
}
