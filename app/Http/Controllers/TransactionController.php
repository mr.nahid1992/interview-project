<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Transaction;


class TransactionController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'account_id' => 'required|numeric',
            'amount' => 'required|numeric|min:0.01',
            'type' => 'required',
            'date' => 'required|date',
        ]);
        $account_id = $request->account_id;
        $account_info = Account::findOrFail($account_id);
        if ($request->type == 'debit') {
            $account_info->balance += $request->amount;
        } else {
            $account_info->balance -= $request->amount;
        }
        $account_info->save();

        $transaction = new Transaction();
        $transaction->account_id=$account_id;
        $transaction->amount=$request->amount;
        $transaction->type=$request->type;
        $transaction->date=$request->date;
        $transaction->save();
        return redirect()->route('accounts.index')->with('success', 'Transaction added successfully!');
    }
}
