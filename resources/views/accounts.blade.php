@extends('layouts.app')
<style>
    .add-trans{
        border: 1px solid grey;
        padding:2rem; border-radius: 12px;
        background-color:#00000005;
    }
</style>
@section('content')
    <div class="container">
        @if(session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif
        <div class="row">
            <div class="col-md-6 well">
                <h3>Accounts</h3>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr class="table-primary ">
                        <th>Name</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td>{{ $account->name }}</td>
                            <td>{{ $account->balance }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6" >
                <div class="add-trans">
                    <h3>Add Transaction</h3>
                    <form method="POST" action="{{ route('transactions.store') }}" >
                        @csrf
                        <div class="form-group">
                            <label for="account_id">Account:</label>
                            <select name="account_id" id="account_id" class="form-control" required>
                                <option value="">-- Select an account --</option>
                                @foreach($accounts as $account)
                                    <option value="{{ $account->id }}">{{ $account->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="amount">Amount:</label>
                            <input type="number" name="amount" id="amount" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="type">Type:</label>
                            <select name="type" id="type" class="form-control">
                                <option value="debit">Debit (+)</option>
                                <option value="credit">Credit (-)</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="date">Date:</label>
                            <input type="date" name="date" id="date" class="form-control" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
