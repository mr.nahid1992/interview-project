<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AccountController;
use App\Http\Controllers\TransactionController;



Route::get('/', [AccountController::class, 'index'])->name('accounts.index');
Route::post('/transaction', [TransactionController::class, 'store'])->name('transactions.store');
